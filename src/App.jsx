import './App.css';
//Component Imports
import Header from './components/Header';
import Footer  from './components/Footer';
import CreateArea from './components/CreateArea';
import Note from './components/Note';

import { useState } from 'react';

function App() {
  const[notes, setNotes] = useState([]);

  function addNote(newNote) {
     setNotes(prevNotes => {
      return [...prevNotes, newNote];
     })
    
  }

  function deleteNote(id){
    setNotes(prevNotes => {
      return prevNotes.filter((noteItem, index) =>{
         return index !== id;
      })
    });
  }

  return (
    <div className="App">
      <Header />
      <CreateArea onAdd={addNote} />
      {notes.map((note, index) =>{
        return <Note key={index} id={index} title={note.title} note={note.content} onDelete={deleteNote} />
      })}
      <Footer />
    </div>
  );
}

export default App;
